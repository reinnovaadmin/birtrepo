* Introduction
 
        Here I included link to installation of eclipse application with birt tool to the project page.
		Downloaded eclipse luna version 4.4 and BIRT tool version 4.4
	
* Installation

	    install eclipse luna standard version 4.4 through the below link
		
		https://www.eclipse.org/downloads/packages/release/luna/r/eclipse-standard-44
		
		
		install birt tool link
		
		https://download.eclipse.org/birt/downloads/drops/I-R1-4.8.0-201805221921/  (or)
                              	

		https://archive.eclipse.org/birt/downloads/drops/I-R1-4.8.0-201805221921/
		
		you can use any one link to install birt tool,this can also provide eclipse luna version 4.4 
		
		
		choose the given directory for BIRT
		
		birt-report-designer-all-in-one-4.8.0-20180522-win32.win32.x86_64.zip  
	
		
* Configuration
        
    1)    After completion of  both eclipse and birt installation.
	2)	  Open eclipse-standard-4.4  -> go to help  -> install new software 
	3)    Install dialogue box should appear 
	4)	  click add button 
	5)    Add Repository
		
		  give -> name     : BIRT 4.4 UpdateSite
		          location : http://download.eclipse.org/birt/update-site/4.4/         (download location)
		
    6)    click select all and click next
    7)    Review the items to be installed and click next
	8)    Accept the terms and click finish
	
	
    9)    check after installation open eclipse 
    10)   file -> new -> others -> BIRT( Business Intelligence Reporting Tool)
         and open report design prespective